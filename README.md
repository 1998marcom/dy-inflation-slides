# dy-inflation-slides

![build status](https://gitlab.com/1998marcom/dy-inflation-slides/badges/master/pipeline.svg?style=flat)


Latest pdf downloadable [here](https://gitlab.com/1998marcom/dy-inflation-slides/-/jobs/artifacts/master/raw/dy-inflation_Malandrone_slides.pdf?job=build).

These are the slides for the final exercise of the course of Complex Systems in UNIPI
