\documentclass{beamer}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                %%
%%             HEADER             %%
%%                                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{anyfontsize}

%%%%%%% BEAMER SETTINGS
\usetheme[width=2.0cm]{Berkeley}   % bianco-azzurro, molto leggero. 
\usecolortheme{seahorse}

\setbeamertemplate{title page}[default][rounded=true,shadow=true]
\setbeamercolor{frametitle}{fg=structure,bg=white}


\makeatletter
\setlength{\beamer@headheight}{0pt}
\makeatother

%\setbeamertemplate{frametitle}[default][center]   % centrare il titolo 

\setbeamertemplate{enumerate items}[circle]
\setbeamertemplate{itemize items}[circle]
%\setbeamertemplate{section in toc}[circle]

\setbeamertemplate{section in toc}[circle]
\setbeamerfont{section in sidebar}{size=\fontsize{7}{10}}
\setbeamerfont{author in sidebar}{size=\fontsize{8}{9}}
\setbeamerfont{title in sidebar}{size=\fontsize{10}{11}}
\setbeamertemplate{frametitle}{
	\vspace*{3mm}
	\insertframetitle
}


%%%%%%% PACKAGE IMPORTS
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{pbox}
\usepackage{url}
\usepackage{anyfontsize}


%%%%%%% COMMONS DEFINITIONS
\newcommand\dd{\partial}


%%%%%%% BIBLIOMERDA
\usepackage[         
    backend=biber,   
    style=numeric,
    citestyle=phys,  
    sorting=ynt      
]{biblatex}
\graphicspath{  
    {images/}   
    {../images/}
}
 
\addbibresource{main.bib} %Imports bibliography file  
\setbeamertemplate{bibliography item}{\insertbiblabel}
\renewcommand*{\bibfont}{\normalfont\footnotesize}
                                                      
%\setbeamertemplate{bibliography entry article}{}     
%\setbeamertemplate{bibliography entry title}{}       
%\setbeamertemplate{bibliography entry location}{}    
%\setbeamertemplate{bibliography entry note}{}  


%%%%%%% MAKETITLE INFO
\title[DY model]{
	Drăgulescu-Yakovenko model and inflation\\
	{\normalsize
		Statics and dynamics of DY and related models\footnote{\scriptsize
			Available online the source code of the models\cite{repo-conti} and 
			of these slides\cite{repo-slides}.
			\vspace{10pt}
		}
	}
}
\subtitle{} %TODO
\author{Marco Malandrone}
\institute[UNIPI]{
	Università di Pisa \\[3mm] 
	Corso di Sistemi Complessi \\
	Prof. Riccardo Mannella
}
\date{17$^\text{th}$ October 2022}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                %%
%%              BODY              %%
%%                                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
	
	\frame{\titlepage}
	
	
	\section{Introduction}

	\begin{frame}{Abstract and goals}
		We explore the statics and the dynamics of some statistical mechanics
		model that may have a monetary interpretation:
		\begin{itemize}
			\item Drăgulescu-Yakovenko models
			\item Random multiplicative exchanges
			\item Mixed models from the two previous categories
		\end{itemize}
		\vspace{20pt}
		We'll also try to insert a partial portrait of inflation inside this
		models and observe its impact on the physics of these dynamical systems.
	\end{frame}
	
	\section{DY model}

	\begin{frame}{The Drăgulescu-Yakovenko model} 
		\begin{itemize}
			\item First discussed by Drăgulescu-Yakovenko in \cite{dragulescu2000}
			\item Discrete time dynamical system.
			\item Has time-reversal symmetry.
		\end{itemize}
		\begin{block}{The setting}
			$N$ agents, each given an initial amount $m_0$ of coins.
		\end{block}
		\begin{block}{A step of the dynamical system}
			\begin{enumerate}
				\item Pick 2 agents at random, a ``winner'' $a$ and a ``loser'' $b$
				\item If $b$ has at least 1 coin, it gives exactly one coin to $a$
			\end{enumerate}
		\end{block}
		Other variations that preserve the time-reversal symmetry are also
		discussed in \cite{dragulescu2000}.
	\end{frame}

	\begin{frame}{DY dynamics}
		\begin{block}{Fokker-Plank equation ($m>0$)\cite{tesi-eterno}}
			\centering\footnotesize\vspace{-10pt}
			\begin{equation}
				\frac{\dd P(m)}{\dd t} = \alpha_l P(m+1) - (\alpha_w+\alpha_l)
				P(m) + \alpha_w P(m-1) 
			\end{equation}
			$\alpha_l = \frac1N \quad \alpha_w = \frac1N(1-P(0))$
			\begin{equation}
				N \frac{\dd P(m)}{\dd t} = P(m+1) - 2P(m) + P(m-1) \; +\; P(0)(P(m)-P(m-1))
			\end{equation}
			\begin{equation}
				\frac{\partial P(m)}{\partial t} = \partial_m (A(m)P(m) + \partial_m (B(m)P(m)))
			\end{equation}
			$ A(m)=\big\langle\frac{\Delta m}{\Delta t}\big\rangle=-rP(0)\quad
			  B(m)=\big\langle\frac{(\Delta m)^2}{2\Delta t}\big\rangle=r $
		\end{block}
		\begin{center}
			\includegraphics[width=\textwidth]{dy-dynamics.png}
		\end{center}
	\end{frame}

	\begin{frame}{DY statics}
		\begin{block}{Equilibrium distribution\cite{tesi-eterno}}
			The system shares the same evolution dynamics of the microcanonical
			ensemble as:
			\begin{itemize}
				\item There's a conserved exchanged quantity, bounded from below for
					each agent.
				\item The dynamics does not differentiate between microstates
					(hence all microstates are equal)
			\end{itemize}

			\begin{equation}
				P(m) = \frac1Te^{-m/T}
			\end{equation}

		\end{block}
		\begin{center}
			\includegraphics[width=\textwidth]{dy-statics.png}
		\end{center}
	\end{frame}

	\begin{frame}{Non integer exchanges}
		\begin{alertblock}{Q: How to simulate non-integer exchanges?}
			\begin{itemize}
				\item A1: hardware floats $-$ but they suffer a few problems
				\item A2: big integers $-$ but if a fixed integer $I$ is chosen, all
					wealths will belong to $m_0+kI$
				\item A3: In \cite{dragulescu2000},\cite{tesi-eterno}, a random 
					integer in the range from 0 to a fixed
					big integer. Still some discreteness scent.
			\end{itemize}
		\end{alertblock}
		\begin{exampleblock}{
				A: Random integers from a poissonian with mean $\lambda$.
		}
			\begin{itemize}
				\item Equilibrium distribution insensitive to the dynamics, as long as
					it does not discriminate between microstates.
			\end{itemize}
			\[P(m) = \frac1Te^{-m/T}\]
		\end{exampleblock}
	\end{frame}

	\begin{frame}{Non integer exchanges}
		\centering
		\includegraphics[width=1.05\textwidth]{dy-statics+dy-poissonian-statics.png}
	\end{frame}
	\begin{frame}{Non integer exchanges}
		\centering

		\begin{block}{Boltzmann-Gibbs equation}
			\fontsize{7}{0}\selectfont
			\begin{equation*}
				\frac{\partial P(m)}{\partial t} =
				\sum_{\Delta=1}^{\infty}\alpha_{l,\Delta}P(m+\Delta)
				-\Bigg(\sum_{\Delta=1}^{m}\alpha_{l,\Delta}+\sum_{\Delta=1}^{\infty}\alpha_{w,\Delta}\Bigg)P(m)
				+\sum_{\Delta=1}^{m}\alpha_{w,\Delta}P(m-\Delta)
			\end{equation*}
			\begin{align*}
				\alpha_{l,\Delta} &= \alpha_l \alpha_\Delta 
								  = \frac1N\mathcal P_\lambda(\Delta)\\
				\alpha_{w,\Delta} &= \alpha_l \alpha_\Delta \;\mathbb P(m_l\geq\Delta)
								  = \frac1N\mathcal P_\lambda(\Delta)
								    \Bigg(\sum_{m=\Delta}^{\infty}P(m)\Bigg)
			\end{align*}
		\end{block}
	\end{frame}
	\begin{frame}{Non integer exchanges}
		\includegraphics[width=1.05\textwidth]{dy-dynamics+dy-poissonian-dynamics.png}
	\end{frame}

	\section{Statics and dynamics of money inflation in a DY model}
	\begin{frame}{Inflation: introductory remarks}
		\begin{block}{Part 1: greater average transaction size}
			\begin{itemize}
				\item We are not tracking products value, only transactions.
				\item We expect inflation to be somewhat related to prices, 
					or, in other terms, to the average
					size of a transaction
			\end{itemize}
		\end{block}

		\begin{block}{Part 2: larger amount of circulating coins}
		\begin{itemize}
			\item Larger supply of money correlates with inflation but they are
				different phenomena.
			\item Injecting large sums of money in a system, violating the
				conservation law of money, is one of the common ways to induce
				money devaluation.
		\end{itemize}
		\end{block}
	\end{frame}
	\begin{frame}{Part 1: greater transaction size - STATICS}
		\begin{itemize}
			\item Any time-reversal symmetric exchange dynamics does not change
				the equilibrium distribution.
		\end{itemize}
		\vspace{-10pt}
		\includegraphics[width=1.05\textwidth]{dy-poissonian-statics+dy-level1-statics-1.png}
	\end{frame}
	\begin{frame}{Part 1: greater transaction size - DYNAMICS}
		\begin{itemize}
			\item The Boltzmann-Gibbs equation gets a modified parameter
				$\lambda$: larger drift and diffusion.
		\end{itemize}
		\vspace{-10pt}
		\includegraphics[width=1.05\textwidth]{dy-poissonian-dynamics+dy-level1-dynamics-1.png}
	\end{frame}
	\begin{frame}{Part 2: larger supply of money}
		\begin{block}{STATICS}
			\begin{itemize}
				\item More money simply increases the temperature of the
					system
			\end{itemize}
			\[P(m)=\frac1Te^{-m/T}, \quad T=m_0\]
		\end{block}
		\begin{block}{DYNAMICS}
			\begin{itemize}
				\item Except for renormalizing the amount of total money,
					injecting money somewhere in the distribution means
					perturbing it.
				\item As shown by M. Eterno\cite{tesi-eterno}, relaxation time
					$t_{eq}=\frac{N}{4}$
			\end{itemize}
		\end{block}
	\end{frame}
	\section{Random multiplicative exchanges}
	\begin{frame}{Random multiplicative exchanges}
		\begin{itemize}
			\item First discussed by S. Ispolatov, P. L. Krapivsky, and S.
				Redner in \cite{rme-paper}
			\item Discrete time dynamical system
			\item Violates temporal reverse symmetry
			\item Has a gamma distribution as equilibrium distribution
		\end{itemize}
		\begin{block}{The setting}
			N agents, each given an initial amount $m_0$ of coins.
		\end{block}
		\begin{block}{A step of the dynamical system}
			\begin{enumerate}
				\item Pick 2 agents at random, a “winner” $a$ and a “loser” $b$
				\item Extract a random integer $\Delta$ from a poissonian with mean
					$\lambda=\alpha m_b$, where $\alpha$ is a fixed parameter.
				\item If $b$ has at least $\Delta$ coins, it gives to $a$
					exactly $\Delta$ coins.
			\end{enumerate}
		\end{block}
	\end{frame}
	\begin{frame}{Random multiplicative exchanges}
		\includegraphics[width=1.05\textwidth]{rme-statics.png}
		\includegraphics[width=1.05\textwidth]{rme-dynamics.png}
	\end{frame}
	\section{Statics of money inflation in a mixed model}
	\begin{frame}{A mixed model for price increases}
		\begin{itemize}
			\item Discrete time dynamical system
			\item Violates temporal reverse symmetry
		\end{itemize}
		\begin{block}{The setting}
			N agents, each given an initial amount $m_0$ of coins.
		\end{block}
		\begin{block}{A step of the dynamical system}
			\begin{enumerate}
				\item Pick 2 agents at random, a “winner” $a$ and a “loser” $b$
				\item Extract a random integer $\Delta$ from a poissonian with mean
					$\lambda=\alpha m_b+\beta$, where $\alpha$ and $\beta$ are
					fixed parameters.
				\item If $b$ has at least $\Delta$ coins, it gives to $a$
					exactly $\Delta$ coins.
			\end{enumerate}
		\end{block}
		\begin{itemize}
			\item we expect $\beta$ to be related to prices
			\item we expect $\alpha$ less related with real prices, whatever
				this may mean.
		\end{itemize}
	\end{frame}
	\begin{frame}{Stable money supply, $\beta$ increases}
		\includegraphics[width=1.05\textwidth]{mixed-statics+mixed-beta-1.png}
	\end{frame}
	\begin{frame}{Fixed $\beta$, increased money supply}
		\includegraphics[width=1.05\textwidth]{mixed-statics+mixed-supply-1.png}
	\end{frame}
	\section{Conclusions}
	\begin{frame}{Follow ups}
		\begin{itemize}
			\item Possible follow up: investigate the dynamics behind
				perturbations to the mixed model\vspace{20pt}
			\item Possible follow up: let both $\beta$ and the money supply 
				increase, so that the distribution stays the same. Find the 
				relation between money supply increase and $\beta$ variations.
		\end{itemize}
	\end{frame}
	\section*{}

	\begin{frame}[noframenumbering]
		\begin{center}
			\Large
			Thanks for your attention!
		\end{center}
	\end{frame}

	\begin{frame}[noframenumbering]
		\begin{center}
			\large \textbf{Riferimenti} \normalsize
		\end{center}
		\printbibliography
	\end{frame}

\end{document}	
